% Dokumentenklasse fuer Artikel waehlen
\documentclass[
% final,
paper=a4,fontsize=12pt,twocolumn=false,titlepage]{scrartcl}

 \usepackage[left=2.5cm,right=2.5cm,top=2.5cm,bottom=2cm,footskip=1cm]{geometry}

% Zur Einbindung von Grafiken mit \includegraphics
\usepackage[pdftex]{graphicx,color}
\DeclareGraphicsExtensions{.pdf,.jpg,.png,.eps,.ps}
\usepackage{graphicx}

\usepackage[english,ngerman]{babel}
\usepackage[style=verbose-ibid,isbn=false,doi=false,backend=biber]{biblatex}
\addbibresource{references.bib}
\AtEveryCitekey{
% \clearfield{isbn}% } \AtEveryBibitem{% \clearfield{isbn}%
}

% Korrekte Umsetzung von Umlauten
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}

% Times fonts
\usepackage{txfonts}

% Todo
\usepackage[textsize=small,obeyFinal]{todonotes}
\presetkeys{todonotes}{fancyline}{}

\usepackage{hyperref}

\usepackage[autostyle]{csquotes}

\usepackage{verbatim}

\usepackage[stable]{footmisc}

% Micro-typographic enhancements
\usepackage{microtype}

% Einrueckung der ersten Zeile eines Absatzes
\setlength{\parindent}{0em}

% Abstand zwischen Absaetzen
\setlength{\parskip}{1.5ex plus0.5ex minus0.5ex}

% Seitenstil
\usepackage{scrpage2}
\pagestyle{useheadings}

% Silbentrennungsliste
\hyphenation{}

\usepackage{setspace}
\spacing{1.5}
\begin{document}

\author{Simon-Martin Schröder}
\title{Autonome Fahrzeuge}

\makeatletter \hypersetup{hidelinks,linktoc=all,pdfauthor={\@author},
pdftitle={\@title}, pdfsubject={\@subject}} \makeatother

% -- Titelseite und Inhaltsverzeichnis erzeugen --------

\hypersetup{pageanchor=false}
\begin{titlepage}
\pagenumbering{gobble}
\centering
Christian-Albrechts-Universität -- Philosophisches Seminar\\[3cm]

\textbf{- Essay -}\\[1cm]

{\large Driverless cars}

\vfill

\raggedright
Risikoethik\\
Moritz Riemann, M.A.

Vorgelegt von: 

Simon-Martin Schröder\\
Mat.-Nr. 5279\\

Weißenburgstr. 45\\
24116 Kiel\\
Tel: 0151 18901404\\
E-Mail: martin.schroeder@nerdluecht.de
\end{titlepage}
\hypersetup{pageanchor=true}

% Seitennummerierung
\pagenumbering{arabic}

\begin{quote}
Eine Minute, bevor ein glücklicher Mann ein Kind tötet, ist er noch glücklich, und eine Minute, bevor eine Frau vor Entsetzen
aufschreit, kann sie noch mit geschlossenen Augen vom Meer träumen, und während der letzten Minute im Leben eines Kindes können
die Eltern dieses Kindes in einer Küche sitzen und auf den Zucker warten und von den weißen Zähnen ihres Kindes sprechen und von
einer Bootsfahrt, und das Kind selbst kann eine Pforte schließen und sich anschicken, mit ein paar Stückchen Zucker in einem
weißen Papier in seiner rechten Hand eine Landstraße zu überqueren, und es kann diese ganze letzte Minute lang nichts anderes vor
Augen sehen als einen langen, blanken Fluß mit großen Fischen darin und einen breiten Kahn mit stillen Rudern.

Nachher ist alles zu spät.\footcite{dagermann48}
\end{quote}

[Das in diesem Essay verwendete generische Maskulinum schließt alle Personen
ungeachtet ihrer Geschlechtszugehörigkeit ein, sofern diese nicht ausdrücklich
genannt wird.]

In der Kurzgeschichte \emph{Ein Kind töten} von Stig
Dagerman geht es um einen Autounfall, bei dem ein Kind
tödlich verunglückt, weil beide -- das Kind und der Fahrer -- unaufmerksam sind.
Wer sich selbst beim Autofahren beobachtet, bemerkt sicher zahlreiche
Situationen, in denen nur nichts passiert, weil Murphy's Law doch nicht so
unbarmherzig ist, wie immer behauptet wird. Fehlender Blick in den Spiegel:
Unfall mit Personenschaden. Langeweile im Stau: Auffahrunfall. Müde auf der
Heimfahrt: Aus der Kurve geflogen. Der Mensch wird von vielen Faktoren
beeinflusst und das Lenken des Fahrzeugs ist fast nie die einzige Aufgabe, die
er auf dem Fahrersitz erfüllt. Wäre es nicht besser, wenn der Fahrer immer
aufmerksam und nie müde wäre, sich nur auf das Fahren konzentrieren müsste und
immer die komplette Umgebung des Fahrzeug im Blick hätte?

In dieser Arbeit wird die Ansicht vertreten, dass es aus risikoethischer Sicht
geboten ist, von Menschen gelenkte Fahrzeuge durch fahrerlose abzulösen. Dies
wird mit der Unzulänglichkeit menschlicher Fahrer sowie den neuartigen Möglichkeiten
hinsichtlich Mobilität und Verkehr begründet.

Eine vollständige Verbannung menschlicher Fahrer aus dem Straßenverkehr ist die
höchste Stufe von automatisiertem Fahren. Reisende sind alleinig Fahrgäste und
von ihnen werden abseits der Zielvorgabe keine Eingriffe in die Steuerung des
Fahrzeugs erwartet. Ein Computer übernimmt unbeschränkt die Kontrolle des
Fahrzeugs (Steuern, Bremsen, Beschleunigen, Überwachen von Fahrzeug und Straße),
die Planung von Manövern (bspw. Spurwechsel) und reagiert selbständig auf
Ereignisse.\footcite{sae:automated_driving}

\todo[inline]{Im Folgenden wird zunächst eine Vision von fahrerloser Mobilität
entfaltet, die anschließend auf möglichen Schwachstellen und Stärken untersucht und
schließlich zusammenfassend bewerted wird.}

\todo[inline]{Motto: Selbstfahrende Fahrzeuge! ABER...}

\section{[Antithese]}
Zunächst wird man die Forderung nach einem fahrerlosen Verkehr für reinen
Technikenthusiasmus halten.
Nur weil etwas technisch möglich ist, muss es nicht wünschenswert sein. Im
weiteren Verlauf wird allerdings deutlich werden, dass dieser Vorwurf nicht
haltbar ist und die Forderung von starken Argumenten unterstützt wird.

Ein entscheidendes Argument gegen die fortschreitende Verschiebung der
Verantwortung für das Wohlergehen von Verkehrsteilnehmern ist die Möglichkeit
des technischen Versagens. Heutige Fälle von technischem Versagen beziehen sich
auf Systeme, die nach festen Regeln Entscheidungen von Menschen umsetzen.
Fehlfunktionen in diesen Systemen lassen sich nie vollständig ausschließen,
schaden meiner These aber nicht, da ihr Auftreten unabhängig vom Entscheider
ist: Ob ein Mensch oder eine Maschine eine Entscheidung trifft, das Ergebnis der
Fehlfunktion einer ausführenden Systemkomponente bleibt das gleiche. Relevant
hingegen sind Fehler des Entscheiders.
Im herkömmlichen Sinne sind dies Menschen, im Fall von intelligenten Maschinen
wie autonomen Fahrzeugen aber auch die Maschine selbst.

PlaneCrashInfo.com\footcite{planecrashinfo:cause}, eine Datenbank über
Flugunfälle, macht Pilotenfehler für 60\% und mechanische Fehler für 18\% der
Unfälle verantwortlich\footnote{Dabei beinhaltet die Kategorie
\enquote{Mechanische Fehler} auch Design- und Wartungsfehler, die auf
menschliche Fehler zurückzuführen sind}.
Der heutzutage übliche Grad der Automatisierung ermöglich es, dass einzelne
Personen Prozesse steuern, die das Leben vieler anderer Menschen berühren.
Geringfügige Bedienfehler können so zu großem Leid führen. Dies war etwa der
Fall beim Zugunglück von Bad Aibling, das sich erst dadurch ereignen konnte,
dass der Fahrdienstleiter zunächst eine eingleisige Strecke für zwei Züge
freigab und seine anschließende Wahrnung die Lokführer nicht erreichte, weil er
die falsche Notruftaste drückte\footcite{spiegel:falschen-knopf}. Der derzeitige
Stand der Automatisierung hat hat also zunächst dazu geführt, dass die
Entscheidungsgewalt in den Händen sehr weniger \emph{Personen} liegt.
Ausgedehnte technische Systeme affizieren ohnehin schon eine große Zahl von
Menschen. Die Übertragung der Entscheidungsgewalt von Personen auf Maschinen
würde die Zahl der affizierten Personen nicht beeinflussen.

Im späteren Verlauf werde ich argumentieren, dass Maschinen auch angesichts der
Möglichkeit einer Fehlfunktion die besseren Entscheider sind.

Jedes technische System lässt sich manipulieren. Je komplexer es ist,
desto weniger offensichtlich ist eine Manipulation. Manipulationen im ausführenden
Teil eines technischen Systems sind aber wiederum unabhängig vom
Entscheider. Gleiches gilt für die die Fälschung von Daten, auf deren Basis
Entscheidungen gefällt werden. Die Auswirkungen einer Manipulation im
sensorischen oder motorischen Teil eines technischen Systems sind unabhängig vom
Entscheider dieselben.

Die Manipulation einer entscheidenden Systemkomponente hingegen ist äquivalent
zur Beeinflussung eines menschlichen Entscheiders. Ist ein Angreifer physisch
anwesend, kann er sehr einfach Einfluss auf Mensch und Maschine nehmen und den
menschlichen Entscheider sogar ersetzen. Ein einfaches Beispiel wäre der
Diebstal eines Kraftfahrzeugs. Durch Sicherungseinrichtungen kann dieser Fall
häufig bemerkt und auch verhindert werden. Doch auch aus der Ferne kann er
menschliche und maschinelle Entscheider beeinflussen (etwa durch Erpressung oder
Bestechung).
Letztendlich ist absolute Sicherheit weder im einen noch im anderen Fall
möglich. Ein modulares, dezentrales und redundantes System, dessen Parameter und
Funktion in angemessener Weise durch technische Überwachungssystem und
menschliche Beobachter (die aber selbst keine operativen Entscheidungen
fällen) überwacht werden, hat jedoch eine größere Chance einer böswilligen
Manipulation zu widerstehen als ein einzelner Mensch.

Um ihre zuverlässige Funktionalität zu erhalten, dürfen Reparaturen an autonomen
Fahrzeugen nur professionell durchgeführt werden. Vor allem sensorische und
entscheidende Komponenten müssen vor fremdem Zugriff geschützt werden. Das mag
zwar bedeuten, dass ein Fahrzeughalter seinen Wagen nicht mehr selbst reparieren
kann, doch dies ist auch schon bei gegenwärtigen Fahrzeugen die Regel und durch
gesetzliche Verbote reglementiert, da bspw. Tuning die Einschätzbarkeit von
Fahrzeugen durch Dritte verringert und damit die Unfallgefahr erhöht.
Die weit verbreitete Praxis des Manipulierens der Fahrzeugleistung oder -optik
durch Tuning verrät, wie viel Enthusiuasmus dies dennoch bei Liebhabern
bestimmter, meist sportlicher Fahrweisen auslöst. Eine volle Automatisierung des
Fahrens dürfte bei dieser Personengruppe zunächst auf Ablehnung stoßen.
Allerdings gibt es keinen vernünftigen Grund, einzelnen eine riskante Fahrweise
auf Kosten der Sicherheit Dritter zuzugestehen. Zudem wird ein optimal
entscheidendes Auto sogar in der Lage sein, die sportliche Fahrweise eines
menschlichen Fahrers zu übertreffen, während die Unfallwahrscheinlichkeit
minimiert wird.

Wie oben beschrieben stellen Manipulationen, böswillige als auch wohlmeindende,
ein großes Risiko dar. Mithilfe von entsprechend gestalteten Systemen können sie
jedoch erkannt und verhindert werden. Manipulationen an mechanischen und
elektronischen Komponenten können durch das Fahrzeug selbst erfasst werden,
worauf es reagieren kann, indem es seine Funktion versagt und den Vorfall an
zuständige Stellen weitergeleitet werden.

Die eben erwähnt Vernetzung der Fahrzeuge ist ein elementarer Bestandteil der
Vorteile autonomer Fahrzeuge. Eine Optimierung des Verkehrsflusses kann nicht
durch einzelne selbst fahrene Autos gewährleistet werden, wenn die Vielzahl noch
immer durch menschliche Entscheidungen gelenkt wird. So könnte erst eine
signifikante Anzahl der autonomen Verkehrsteilner Staus vermeiden, sofern sie
untereinander durch Datenaustausch ihre Fahrweisen und Manöver anpassen und
damit verbliebene menschliche Teilnehmer entlasten.
Dennoch stellt diese Vernetzung verständlicherweise auch ein größeres Risiko
durch Manipulationen dar. Während schon ein einzelnes manipuliertes Fahrzeug
einen großen Schaden anrichten kann, kann die Wirkung einer koordinierten
Manipulation vieler Fahrzeuge oder gar eines zentralen Verkehrssteuerungssystems
verheerend sein. Daher muss trotz einer Vernetzung jedes Fahrzeug möglichst
autonom entscheiden und Kommunikationsschnittstellen von Fahrzeugen, heutigen
und zukünftigen, müssen qbesonders geschützt werden. Es dürfen nur definierte
Daten übertragen werden und jedes Fahrzeug muss alle zur Verfügung stehenden
Daten mit dem nötigen Misstrauen betrachten und anhand eigener Informationen
verifizieren. Auf diese Weise kann das Risiko einer großangelegten Manipulation
minimiert werden.

Stellt man sich all die Möglichkeiten vor, mit denen ein automatisiertes
Fahrzeug von Nutzen sein kann, Geschwindigkeitsanpassung, Bremsverhalten,
Abstandsmesser, Benzinverbrauch, sogar medizinische Überprüfung der Fahrgäste
oder Fahrer, so sind all diese Vorteile anfällig für Manipulationen, die
verheerende Folgen haben können.

Entgegen der verbreiteten Furcht vor Fehlfunktion oder Manipulation wurden
bisher jedoch sehr gute Erfahrungen mit fahrerlosen Fahrzeugen gemacht.
Die Metro Kopenhagen setzt selbstfahrende Züge ein, die nur in
Ausnahmesituationen von den anwesenden \enquote{Metro Stewards} gesteuert
werden.\footcite{Jensen2002} Denmarks Statistik, das dänische Äquivalent zum
Statistischen Bundesamt, verzeichnet seit ihrer Eröffnung am 19.~Oktober 2002
keinen einzigen Unfall, bei dem Menschen verletzt oder gar getötet wurden. Dem
gegenüber stehen sieben getötete und 20 verletzte Opfer des landesweiten
konventionellen Schienennetzes in den Jahren 2002 bis 2003 und ähnliche Zahlen
in den nachfolgenden Jahren.\footcite{dst.dk:traffic-accidents} Googles
\emph{Self-Driving Car} erlebte erst in diesem Jahr den ersten Unfall, der nicht
von Menschen verschuldet war. Zuvor war es bei einer Fahrleistung von ca. 2 Mio.
Kilometern in 17 fremdverschuldete Unfälle mit insgesamt einer leichtverletzten
Person verwickelt.\footcite{wired:first-crash} Im normalen Straßenverkehr wurden
im Jahr 2013 auf US-amerikanischen Straßen auf die gleiche Fahrleistung
gerechnet ca. 1000 Menschen verletzt und 14 getötet.\footcite{nhtsa2014}


\begin{comment}
- OK Reiner Technikenthusiasmus
- Risiko technisches Versagen. -> Technisches Versagen auf menschliches Versagen zurückzuführen. Viele-Augen-Prinzip schützt.
- Risiko Manipulation \url{https://www.ted.com/talks/avi_rubin_all_your_devices_can_be_hacked}
	- Individuelles Fahrzeug
		- Begrenzter Schaden (check)
	- Infrastrukturlenkung
		- Immenser Schaden möglich -> Vermeiden durch Dezentralität (Steuerung im einzelnen Fahrzeug) (Check)
	- "Optimierungen" durch Benutzer -> Kryptographische Signierung von Komponenten (check)
- Kein Fahrspaß -> Situationsangepasst im Rahmen der Sicherheit maximal
sportliche Fahrweise (check)
- Nicht selbst reparieren (Check)
\end{comment}

\begin{comment}
All your devices can be hacked
- Implanted medical devices: ICD (defibrillator)
    - Turn off
    - Trigger fibrillation
- Automobiles
    - internal wired network
        - change speedometer value
        - apply brakes
        - disable brakes
        - install malware
    - wireless network
         - short range: wheel sensor
         - long range: digital radio
    - activate malware via wireless network

"be aware that devices can be compromised, and anything that has software in it is going to be vulnerable. It's going to have bugs."
\end{comment}

\section{[Anforderungen]}
\todo[inline]{TODO: Abschnitt oben integrieren}
\begin{itemize}
    \item Maximale Anzahl von Experten muss ein selbstfahrendes System überprüfen können (OpenSource)
    \item Auf der Straße dürfen sich nur vollständig zertifizierte Systeme bewegen (keine \enquote{Basteleien})
    \item Dezentrale Steuerung: Jedes Fahrzeug entscheidet selbst.
    \item Fahrzeug muss maximal gegen fremden Zugriff abgesichert sein, gleichzeitig muss jede Komponente so öffentlich wie nur möglich einsehbar sein, sodass Gefahren sowohl für die Systemsicherheit als auch die Verkehrssicherheit minimiert werden. -> Öffentliche Aufgabe: Zertifizierung und ständige Überprüfung (inkl. Förderung unabhängiger Forscher)
    
    \item Zulassung autonomer Fahrzeuge an Audit knüpfen.
    \item Dezentralität
    \item Modularität
    \item Redundanz
\end{itemize}

\section{[These]}

\todo[inline]{TODO: Folgende Absätze kombinieren}
Erklärtes Ziel der deutschen Verkehrspolitik ist die Maximierung
der Verkehrsmenge. Eine leistungsfähige Infrastruktur sei die entscheidende
Voraussetzung für Mobilität.\footcite{bmvi:verkehr-mobilitaet} Allerdings ist
Verkehr für eine Reihe von Kosten\todo{welche}\ verantwortlich, die in der Regel
externalisiert werden. Daher spricht sich Becker dafür aus, statt dessen die
individuelle Mobilität zu optimieren und dabei das Verkehrsaufkommen zu
minimieren.\footcite[S. 334f.]{becker2013}
Fahrerlose Fahrzeuge können auf mehrfache Weise zum Erreichen dieses Ziels
beitragen.

Ein Verkehr ohne Fahrer bedeutet eine radikale Veränderung aller Aspekte, die
ihn betreffen. \emph{Verkehr} ist ist das Instrument, das Mobilität ermöglicht.
\emph{Mobilität} erlaubt die Befriedigung von Bedürfnissen, die mit
Ortsveränderungen zusammenhängen. Der Nutzen des Verkehrs kommt in der Regel
allein den Reisenden zugute, während die Kosten (Verkehrswegekosten,
Unfallkosten, Klimakosten, Lärm, Abgase, Trennwirkung, Abfälle, usw.)
externalisiert, d.h. von der gegenwärtigen und zukünftigen Gesellschaft getragen
werden.\footcite{becker2013} Durch fahrerlose Fahrzeuge kann gleichzeitig
sowohl der Zugang zu individueller Mobilität verbessert und damit die Anzahl der
Nutznießer des Verkehrs erhöht als auch das Verkehrsaufkommen und die damit
verbundenen Kosten für Unbeteiligte reduziert werden.

\begin{verbatim}
- Einsparung von Material, Energie und Emissionen -> Risiko Resourcenverschwendung. Welt bleibt bewohnbar
	- sparsames (Verbrauch) & abnutzungsarmes (Verbrauch) Fahren dank Optimierung des Fahrverhaltens
		- optimierte Geschwindigkeit mit langer Vorausplanung
		- Lenkbewegung optimiert hinsichtlich Verschleiß
	- Förderung von Elektromobilität
		- Automatisches Aufladen, zeit- und ortsunabhängig vom Benutzer (z.B. nachts an öffentlicher Ladestation)
- Roboter-ÖPNV potenzieren Nutzen
    - Weniger Fahrzeuge insgesamt nötig
	    - Weniger Emissionen
	    - Weniger Verkehrsflächen (mehr Platz für Erhohlung, Wohnen, ...)
	    - Geringere Verkehrsdichte -> Geringeres Gefahrenpotential für Fußgänger und Radfahrer
    - Günstiger, flexibler, individueller Personennahverkehr
\end{verbatim}

Technik, um Unfälle wie den im einleitenden Zitat beschriebenen zu verhindern,
gibt es schon heute \footnote{Bspw.
\url{https://www.youtube.com/watch?v=u80eraX8IJQ}}. Allerdings sind heutzutage
regulär käufliche Fahrzeuge noch immer auf die Kontrolle durch einen Fahrer
angewiesen. Doch autonome Versuchsfahrzeuge beweisen bereits ihre Überlegenheit
gegenüber von Menschen gelenkten Fahrzeugen.
\begin{verbatim}
https://www.wired.com/2016/02/googles-self-driving-car-may-caused-first-crash/
24.10.16
Google’s cars have driven more than
1.3 million miles since 2009. They can recognize hand signals from traffic
officers and “think” at speeds no human can match. As of January, they had been
involved in 17 crashes, all caused by human error.
\end{verbatim}

\begin{verbatim}
- Menschliches Versagen wird minimiert -> Risikofaktor Mensch
    - Übermüdete Fahrer (nach langer Arbeit, anstrengendes Wochenende)
    - Ablenkung (Telefonieren, Streit, Familienchaos, Aufmerksamkeitserregendes am Straßenrand, Emotionale Erlebnisse zuvor)
    - Emotionen im Straßenverkehr (Ärger über Verkehrsteilnehmer, Stau)
    - Drogen- \& Alkoholkonsum
\end{verbatim}

\url{https://www.ted.com/talks/jennifer_healey_if_cars_could_talk_accidents_might_be_avoidable}
\begin{verbatim}
- Driving is dangerous
- Unpredictable behavior of drivers -> car communication
- Human body not designed for task
	- Single focus of attention
	- Limited sensing range (motor cyclist was seen by many others before passing)
- Sharing position data
- Willingness to share data:
	 - "disconcerting notion, this idea that our cars will be watching us, talking
	 about us to other cars"
	 - Protect privacy
\end{verbatim}

Im Zentrum meiner Argumentation steht der Mensch als unperfekter Fahrer.
Zunächst besteht nur eine Minderheit der Verkehrsteilnehmer aus speziell
ausgebildeten, berufsmäßigen Fahrern, die sich wiederholten Schulungen und Tests
unterziehen, während die Mehrheit aus mehr oder weniger erfahrenen Laien
besteht, die einmalig die Führerscheinprüfung absolviert haben.

Darüber hinaus besitzen Menschen zahlreiche Eigenschaften, die sie
eigentlich als Fahrer eines Kraftfahrzeugs disqualifizieren:

Menschen werden müde. Doch nach einem langen Arbeitstag oder einem
anstrengenden Wochenende ist trotzdem noch die Rückreise zu absolvieren. Auch
das Fahren selbst lässt den Fahrer ermüden. Müde Fahrer können nur verzögert auf
unvorhergesehene Ereignisse reagieren. Im Extremfall schlafen sie am Steuer
ein.

Menschen lassen sich leicht ablenken. Die Aufmerksamkeit des Fahrers liegt nur
in seltenen Fällen vollständig auf der Verkehrssituation. Nebenbei wird
telefoniert oder mit Mitreisenden gesprochen, teilweise sogar gestritten. Eltern
sind häufig von ihren Kindern in Anspruch genommen. Und auch am Straßenrand
können Personen oder Ereignisse die Aufmerksamkeit des Fahrers binden.

Menschen sind emotional. Streit vor oder während der Fahrt, Ärger über
Verkehrsteilnehmer oder -situationen oder auch positive Emotionen wie in der
einleitenden Kurzgeschichte können den Fahrer zu einer unangemessenen Fahrweise
verleiten.

Menschen konsumieren legale und illegale Genussmittel, die ihre Wahrnehmung und
Urteilskraft beeinflussen. Auch benötigen sie manchmal Medikamente mit einer
ähnlichen Wirkung. Obwohl gesetzlich verboten, kommt es vor, dass sie unter diesem
Einfluss am Straßenverkehr teilnehmen.

Darüber hinaus sind Menschen, auch ohne dass sie durch die obigen Faktoren
beeinträchtigt wären, in Hinblick auf ihre gegebenen Wahrnehmungs-,
Kommunikations- und Entscheidungsfähigkeiten nicht besonders gut angepasst an
die Anforderungen des Straßenverkehrs. Die Wahrnehmung der Umgebung findet
überwiegend über den Sehsinn statt, doch die Augen erfassen zu einem Zeitpunkt
nur einen winzigen Teil der Szene. Zusätzlich ist die Sicht aus dem
Fahrzeuginnenraum durch die Karosserie beeinträchtigt. Der Fahrer muss seinen
Fokus in kurzer Folge auf eine große Zahl von Reizen richten. Die Unterscheidung
wichtiger von unwichtigen Reizen fällt besonders jungen Fahrern
schwer.\footcite{Deery2013} Eine Kommunikation mit Führern benachbarter
Fahrzeuge ist in der Regel unmöglich.

Schlussendlich sind menschliche Entscheidungen durch eine
Reihe von kognitiven Verzerrungen beeinflusst. Prominent ist die Überschätzung
der eigenen Fähigkeiten: 80\% der Fahrer erachten sich besser als der
Durchschnitt\footcite{McCormick1986}, was zu einer übermäßig optimistischen und
risikofreudigen Fahrweise führt\footcite[Vgl.][]{Svenson1981}.

\todo[inline]{TODO: Folgende Notizen und Absatz oben integrieren}
\url{https://www.ted.com/talks/chris_gerdes_the_future_race_car_150mph_and_no_driver}
\begin{verbatim}
The future race car — 150mph, and no driver 

Tired/drunk/mentally absent, but overestimate own skills. Familiar for every driver.
State of Nevada granted Google Self driving car driving license.

- Robotic race cars
    - Car should be at least as good as human driver
    - Want a car, that is capapable of avoiding any accident, that can physically be avoided.
    - World's first autonomous drifting car
- Human race car drivers are amazing at driving the fastes line on a race track.
    - Monitor brain activity
    - Path planning: High mental workload
    - counterbalance drifting: low mental workload, instinctive
-> Imitate human instincts to make the car drive in every stituation like the best racing car driver
\end{verbatim}

\begin{verbatim}
- Driving accidents are the #1 cause of death for young people [citation needed]
    - due to human error, not machine error
    -> preventable by machines
\end{verbatim}


Während die meisten Fahrer Laien sind, ist es autonomen Fahrzeugen möglich,
jederzeit wie konzentrierte professionelle Fahrer zu fahren. Oder sogar besser,
wenn man die besseren Möglichkeiten der Wahrnehmung der Umgebung in Betracht
zieht.


\begin{verbatim}
- Gesellschaftlicher Nutzen
	- Zeitersparnis (man kann die Fahrzeit produktiv nutzen)
	- Günstigere Mobilität im Falle von ÖPNV
\end{verbatim}

\todo[inline]{TODO: Folgende Absätze zu einem sinnvollen Ganzen verbinden}
Derzeitige Fahrzeuge im Straßenverkehr erzeugen neben den von der Gesellschaft
getragenen Kosten auch solche, die insbesondere die Fahrer belasten.

Die Zeit, die ein Mensch hinter dem Steuer verbringt, steht ihm nicht für
tatsächlich produktive Tätigkeiten zur Verfügung.

Genießt ein Fahrer nicht das Fahren an sich als Erholung, ist die Zeit hinter
dem Steuer im warsten Sinne verschenkt.

Die Aufmerksamkeit potentiell stundenlang vollständig an eine Tätigkeit zu
binden, deren einziges Ziel der Ortswechsel einer Person ist, erscheint als
Verschwendung von Lebenszeit, sobald Alternative Fortbewegungsmöglichkeiten mit den selben
Vorteilen zur Verfügung stehen, die den Reisenden nicht als Fahrer beanspruchen.
Die Möglichkeiten, diese Zeit zu nutzen sind endlos, der Reisende ist frei in
der Wahl seiner Beschäftigung.

Gleichermaßen ist es eine Verschwendung von Lebenszeit und Straßenkapazität,
wenn jeder Reisende ein einzelnes Fahrzeug selbst steuert.

Dass Reisende ihre komplette Aufmerksamkeit und häufig sogar jeweils ein
einzelnes verwenden, ist eine Verschwendung im mehrfachen Sinne: Zeit, die
anders besser genutzt wäre. Platz für ein Fahrzeug pro Person. Ressourcen, um
jedes einzelne Fahrzeug zu bewegen.

Besser: Nicht fahren, platzsparende verbindbare Wagons, gemeinsamer Antrieb,
s.u.

Die Kapazität vorhandener Straßen ließe sich durch den flächendeckenden Einsatz
von selbstfahrenden Autos um 40\% bis 80\% erhöhen und
Staus weitestgehend vermeiden.\footcite{Pinjari2013} Auf diese Weise kämen
mehr Reisende schneller an ihr Ziel.

Da kein Fahrer benötigt wird, ermöglichen selbstfahrende Fahrzeuge eine höhere
Mobilität für diejenigen, die kein Fahrzeug steuern können oder dürfen. Dies
betriff beispielsweise Kinder, alte Menschen und Menschen mit Behinderung.

Selbstfahrende Autos ermöglichen eine neue Form des Carsharings, bei dem ein
Reisender nur seinen Bedarf melden muss und an Ort und Stelle vom Fahrzeug
abgeholt werden kann, anstatt zunächst zum Fahrzeug gelangen zu müssen.
Ein einzelnes gemeinsam genutztes selbstfahrendes Fahrzeug kann so bis zu zwölf
privat genutzte konventionelle Fahrzeuge ersetzen.\footcite{Fagnant2014} Eine
auf diese Weise verminderte Fahrzeugzahl würde die Verkehrskosten weiter
reduzieren.

\url{https://www.ted.com/talks/wanis_kabbaj_what_a_driverless_world_could_look_like}
\begin{verbatim}
- "Time worth living": Time wasted in jams
	" waste of time, energy and human potential."
- Coordinated approach to leverage traffic problems
- Space is wasted (single driver)
- Transcend endless debate between creating a car-centric society or extensive
mass-transit systems
- Dynamically detatched waggons
- Driverless cities
	- No lanes, no signs, no traffic signs
	- All cars driverless and connected
	- the more robotized our traffic grid will be, the more organic and alive its
	movement will feel.
\end{verbatim}

\url{https://www.ted.com/talks/sebastian_thrun_google_s_driverless_car}
\begin{verbatim}
Google's driverless car 
- Change capacity of highways by 2-3 by using robotic cars that can drive closer together and avoid traffic yams
- 51min per day in traffic wasting time and gasoline
\end{verbatim}


Eine mögliche Sichtweise ist, dass ein Reisender in einem autonomen Fahrzeug
seine Position als Fahrer aufgibt und wird zum reinen Fahrgast wird. Er gibt
einerseits Entscheidungsgewalt ab, erhält aber im Gegenzug die Freiheit, die
Fahrzeit selbstbestimmt zu nutzen.

Autonome Fahrzeuge werden in Zukunft eine neue Art von öffentlichem Nahverkehr
ermöglichen. Schon bietet der Fahrdienstleister Uber experimentelle Fahrten mit
autonomen Fahrzeugen an.\footcite{wired:ubers-self-driving-car} Es ist nur
eine Frage der Zeit, bis es kommunale Angebote gibt. Im Vergleich zum
herkömmlichen öffentlichen Nahverkehr wäre eine individuelle Mobilität auch
Personen ohne eigenes Fahrzeug zugänglich.

\todo[inline]{Gerechte Verteilung von Mobilität an alle, die ihrer bedürfen.
Andersherum erhalten Personen, die kein eigenes Fahrzeug besitzen, das selbe Maß
an Mobilität wie Personen mit eigenem Fahrzeug.}


\section{[Synthese]}

Maximin-Kriterium: Was ist der schlimmste Schaden (unabhängig von der
Wahrscheinlichkeit), der jeweils eintreffen kann?

Vorsorgeprinzip
Natürlich ist die komplette Umstellung auf autonome Fahrzeuge eine radikale
Forderung, deren Umsetzung viel Zeit in Anspruch nehmen würde. 

\begin{verbatim}
https://www.ted.com/talks/jeremy_howard_the_wonderful_and_terrifying_implications_of_computers_that_can_learn
The wonderful and terrifying implications of computers that can learn 

- Deep learning inspired by human brain
    - The more data, the better prediction
    - Learn by example
- Online learning reduces time

- Problem: Human work is replaced by computer's work
    - Computer performance is growing exponentially
- Solution:
    - adjust economical & social structures
    - separate labor from earnings
    - craft-based economy 
\end{verbatim}


\begin{verbatim}
- Vergleich Metro-Systeme
    - Japan: Sekundengenaue Abfahrt
- Zwar: Versagen in der Herstellung möglich, jedoch durch viele Augen erkennbar
\end{verbatim}

\begin{comment}
https://www.wired.com/2015/01/rode-500-miles-self-driving-car-saw-future-boring/
 piecemeal approach also is easier for automakers, and regulators. Making one
swift jump to fully autonomous driving, as Google is pursuing, requires
perfecting all of the technology and considering every possible scenario. The
car must know exactly what to do, everywhere, under every condition. It’s a
massive undertaking, and it’s easy to see why the automakers want to take things
one step at a time.

regulations
\end{comment}

Zusammenfassend lässt sich sagen, dass autonome Fahrzeuge, die für den Einsatz
im Straßenverkehr optimiert sind, dem Menschen im Hinblick auf Wahrnehmungs-,
Kommunikations- und Entscheidungsfähigkeiten überlegen sind\todo{oben beweisen}.
Steigt ihr Anteil im Straßenverkehr, sinkt die Gefährdung von Menschen durch Fehlleistungen
menschlicher Fahrer überproportional\todo{oben beweisen}.
\ldots

Als Fahrgast eines autonomen Fahrzeugs könnte sich der Vater aus der
einleitenden Kurzgeschichte den Tag am Meer ausmalen, die sportliche Fahrt
genießen und sich ganz auf seine Familie konzentrieren.\todo{alles, was Menschen
ablenkt}, während das Kind ungefährdet vom Fluss träumen könnte. Dass sich ihre
Wege kreuzen, könnte von beiden unbemerkt bleiben.

% -- Literaturverzeichnis --------
\newpage
\printbibliography

\end{document}
